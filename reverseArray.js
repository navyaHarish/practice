const prompt = require('prompt-sync')();

let N = prompt('Please enter the size of the array:');

let myArray = [], resultArray = [];

for(let i=0; i<N; i++) {
	myArray[i] = prompt('Enter the element ' + (i+1) + ':');
}

for( let i=0; i<N; i++){
    resultArray.push(myArray.pop());
}

console.log(resultArray);