const prompt = require('prompt-sync')();

let myString = prompt('Please enter a string:');

sortedArray = myString.split('').sort();

function removeDuplicates(arr){
    let uniqueString = [];
    arr.forEach(element => {
        if(!uniqueString.includes(element)){
            uniqueString.push(element);
        }
    });
    return uniqueString;
}

sortedString = removeDuplicates(sortedArray).join('');

console.log(`The sorted string without duplicate characters is '${sortedString}'`);