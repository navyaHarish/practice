const prompt = require('prompt-sync')();

let myString = prompt('Please enter a string:');

function palindrome(myString) {
    let newStr = myString.toLowerCase();
    let reversedString = ``;
    for (let i = 0; i < newStr.length; i++) {
        reversedString = newStr[i] + reversedString;
        
    }
    return newStr === reversedString ? `It's a palindrome` : `It's not a palindrome`; // (a)
 }
  
console.log(palindrome(myString));