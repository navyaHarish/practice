//Program to print out patterns

const prompt = require('prompt-sync')();

let userInput = prompt('Enter the character you wish to use:');

function halfPyramid(userInput,n) {
    for (let i = 0; i <= n; i++) {
        for (let j = 0; j < i+1; j++) {
            process.stdout.write(userInput+' ');
      } 
      process.stdout.write("\n");
    }
  }


function invertedHalfPyramid(userInput,n) {
    for (let i=n; i>=0; i--){
      for (let j = 0; j<i+1; j++) {
        process.stdout.write(userInput+' ');
      } 
      process.stdout.write("\n");
    }
  }


function halfPyramidWithNumbers(n) {
    for (let i=1; i<=n; i++) {
      for (let j=1; j<=i+1; j++) {
        process.stdout.write(`${j} `);
      } 
      process.stdout.write("\n");
    }
  }


function invertedHalfPyramidWithNumbers(n) {
    for (let i = 1; i <= n; i++) {
      for (let j = 1; j <= (n - i + 1); j++) {
        process.stdout.write(`${j} `);
      } 
      process.stdout.write("\n");
    }
  }

function numberPattern(startPoint,NumOfReps){
    count=0
    for( let i=startPoint; count<4;count++){
        for( let j=0; j<count+1; j++){
            process.stdout.write(`${i} `);
        }
        process.stdout.write("\n");
        i++;    
    }

    for (let i=startPoint+NumOfReps-2; count>0; count--){
        for ( let j=0; j<count-1; j++){
            process.stdout.write(`${i} `);
        }
        process.stdout.write("\n");
        i--;
    }
}

function solidDiamond(userInput,n){
    let k = 2*n-2;
    for (let i=0; i<n; i++){
        for (let j=0; j<k; j++){
            process.stdout.write(' ');
        }
        k=k-1;
        for (let j=0; j<i+1; j++){
            process.stdout.write(userInput+' ');
        }
        process.stdout.write("\n");
    }
    k=n-2;
    for (let i=n; i>=0; i--){
        for (let j=k; j>0; j--){
            process.stdout.write(' ');
        }
        k=k+1;
        for (let j=0; j<i+1; j++){
            process.stdout.write(userInput+' ');
        }
        process.stdout.write("\n");
    }
}

function hollowDiamond(userInput,n){
    k = 0; 
    for( let i=1; i<=n+1;i++){   
        for (let j=1; j<=n-i+1; j++){
            process.stdout.write(' ');
        }
        while (k != (2 * i - 1)){
            if (k == 0 || k == 2 * i - 2){
                process.stdout.write(userInput+' ');
            }
            else {
                process.stdout.write(' ');
            }
            k++;
        }      
        k = 0;
        process.stdout.write('\n');
    }
    n--;
    for (let i=n+1; i>0; i--){
        for ( let j=0; j<=n-i+1; j++){
            process.stdout.write(' ');
        }
        k = 0
        while (k != (2 * i - 1)){
            if (k == 0 || k == 2 * i - 2){
                process.stdout.write(userInput+' ');
                }
            else{
                process.stdout.write(' ');
                }
            k++;
        }
        process.stdout.write('\n');
        }
    }
