
const prompt = require('prompt-sync')();

let N = prompt('Please enter the size of the array:');

let myArray = [];
let sumOddIndices = 0;


for(let i=0; i<N; i++) {
    myArray[i] = prompt('Enter the element ' + (i+1) + ':');
    }


  myArray.forEach(function oddIndices(arr,index){
    if (index % 2 !== 0){
        sumOddIndices = sumOddIndices+Number(arr);
    }
    return sumOddIndices;
  });


console.log(`The sum of Odd Indices of [${myArray}] is ${sumOddIndices}`);
