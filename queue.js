const prompt = require('prompt-sync')();

let N = prompt('Please enter the size of the array:');

let myArray = [], frontIndex = 0, lastIndex = N;

for(let i=0; i<N; i++) {
    myArray[i] = prompt(`Enter the ${i+1} element:`);
    }

console.log(`Your input Array is [${myArray}]`)


const push = (element) => {
    myArray[lastIndex] = element;
}

const pop = () => {
    myArray.shift();
}


push(5);
console.log(`[${myArray}]`);
pop();
console.log(`[${myArray}]`);
pop();
console.log(`[${myArray}]`);