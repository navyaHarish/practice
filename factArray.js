
const prompt = require('prompt-sync')();

let N = prompt('Please enter the size of the array:');

let myArray = [];

for(let i=0; i<N; i++) {
	myArray[i] = prompt('Enter the element ' + (i+1) + ':');
}

let resultArray =[];

myArray.forEach(function factorial(n){
    let fact = 1;
    if (n == 0 || n == 1){
      resultArray.push(fact);
    }
    else{
      for(let i = n; i >= 1; i--){
        fact = fact * i;
      }
      resultArray.push(fact);
    }  
  });


console.log(`The factorial array of [${myArray}] is [${resultArray}]`);