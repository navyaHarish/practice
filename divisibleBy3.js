
const prompt = require('prompt-sync')();

let N = prompt('Please enter the size of the array:');

let myArray = [];

for(let i=0; i<N; i++) {
	myArray[i] = prompt('Enter the element ' + (i+1) + ':');
}

let sum = 0;

myArray.forEach(function divideBy3(n){
    if ( n%3===0 ){
      sum = sum + Number(n);
    }
  });


console.log(`The sum of the numbers of the array which are divisible by 3 is ${sum}`)
