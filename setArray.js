const prompt = require('prompt-sync')();

let N = prompt('Please enter the size of the array:');

let myArray = [], mySet = new Set([3,5,6,8]), sum = 0;

for(let i=0; i<N; i++) {
    myArray[i] = prompt('Enter the element ' + (i+1) + ':');
    }

function checkValues(myArray){
    for( let i=0;i<myArray.length; i++){
        for (const k of mySet.keys()) {
            if (k == myArray[i]){
                sum = sum + Number(myArray[i]);   
            }
        }
    }
    return sum;   
}

console.log(`The sum is ${checkValues(myArray)}`);