const prompt = require('prompt-sync')();

let num = prompt('Please enter the number:');


function factorial(n){
    let fact = 1;
    if (n == 0 || n == 1){
      return fact;
    }
    else{
      for(let i = n; i >= 1; i--){
        fact = fact * i;
      }
      return fact;
    }  
  }

console.log(`The factorial of ${num} is ${factorial(num)}`);