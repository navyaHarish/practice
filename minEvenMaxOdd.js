
let evenArray = oddArray = [];
let difference = 0;

const prompt = require('prompt-sync')();

let N = prompt('Please enter the size of the array:');

let myArray = [];

for(let i=0; i<N; i++) {
	myArray[i] = prompt('Enter the element ' + (i+1) + ':');
}

// console.log(myArray);

evenArray = myArray.filter(item => item % 2 == 0);
oddArray = myArray.filter(item => item % 2 !== 0);

// maxOdd = Math.max.apply(null,oddArray);
// minEven = Math.min.apply(null,evenArray);

function maxValue(arr){
    let max = arr[0];
    for (let i = 1; i < arr.length; i++) {
        let value = arr[i];
        max = (value > max) ? value : max;
      }
    return max;
}

function minValue(arr){
    let min = arr[0];
    for (let i = 1; i < arr.length; i++) {
        let value = arr[i];
        min = (value < min) ? value : min;
    return min;
    }
}

// console.log(maxValue(oddArray));
// console.log(minValue(evenArray));

difference = (maxValue(oddArray)) - (minValue(evenArray));
console.log(`The difference between the largest odd number and smallest even number is ${difference}`);
